#!/bin/sh
### NOTE
# This was a very old script when I only used Arch with I3
# It will not work on Void and It won't install dwm
### install needed software
sudo pacman --noconfirm -Syyu
sudo pacman --noconfirm -S wget xorg-server xorg-xinit xorg-xrandr xterm i3 dmenu alacritty unzip zip picom lxappearance base-devel connman wpa_supplicant
### ADDED PIPEWIRE may not need some packages
sudo pacman -S pipewire xdg-desktop-portal pipewire-alsa pipewire-pulse pipewire-jack
systemctl start --user pipewire-pulse.service
echo you may want to reboot
#### optional packages
sudo pacman --noconfirm -S redshift light dunst polkit lxsession playerctl feh
### Network manager
#sudo pacman -S networkmanager network-manager-applet
sudo systemctl enable connman.service
### sets up starting X after login (replaces display manager)
cp .bash_profile ~/.bash_profile
cp .xinitrc ~/.xinitrc
### yay
### Install yay from git
cd /tmp
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg --noconfirm -si
cd ..
### clean /tmp folder
rm -rf yay
### polybar
### with yay
yay --noconfirm -S polybar connman-ui-git connman-gtk
### without yay
#git clone https://aur.archlinux.org/polybar.git
#cd polybar
#makepkg -si
cd ..
cp -a dotfiles/. /home/$USER
