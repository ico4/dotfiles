#! /bin/sh
feh --bg-scale ~/.config/wallpapers/artix.png &
lxpolkit &
picom &
while true; do
	xsetroot -name "$(acpi -b | awk '{print $4}' | sed s/,//)  |  $(date)"
	sleep 55
done 
