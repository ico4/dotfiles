
#
# ~/.bashrc
#
# if you add an alias go in a terminal using bash and write
# source ~/.bashrc
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#PS1='[\u@\h \W]\$ '
#PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '

export EDITOR="nvim"
# untar
alias untar='tar -xvf'
alias untargz='tar -xvzf'
alias untarbz2='tar -xvjf'
# grep
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
# ls
alias ls='ls --color=auto'
alias ll='ls -al'
alias la='ls -a'
## weather stuff
alias weather='curl wttr.in'
alias wea='curl wttr.in/?format=3'
## makes unimatrix output cmatrix
#alias matrix='unimatrix -n -s 96 -l'
##  wifi password
alias wifitest='sudo grep -r ''^psk='' /etc/NetworkManager/system-connections/'
#shutdown and reboot alias
alias shutdown='shutdown now'
##vim
#alias vim='nvim'
## ping
alias pinst='ping -c 3 gnu.org'
######################
##### dumb stuff #####
######################
# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
alias meme_doge='curl e.xec\.sh/doge'

# update grub
alias updategrub='grub-mkconfig -o /boot/grub/grub.cfg'
# pacman alias
alias spu='sudo pacman -Syyu'
alias sps='sudo pacman -S'
alias spr='sudo pacman -R'
alias spp='sudo pacman -Rncs'

### Trackpoint
### To find Trackpoint run the command:
## xinput | grep Track
### You should then find something like
## ⎜   ↳ TPPS/2 IBM TrackPoint

#xinput --set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Profile Enabled" 1, 0

### Set speed from 0.0 to 1.0

#xinput --set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" 0.0

